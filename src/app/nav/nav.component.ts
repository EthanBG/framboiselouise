import { Component, OnInit } from '@angular/core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { StorageService } from '../services/storage/storage.service';

import { HttpClient } from '@angular/common/http';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {NgIf} from "@angular/common";
import {MatMenuModule} from "@angular/material/menu";

@Component({
  selector: 'app-nav',
  standalone: true,
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
  imports: [
    MatButtonToggleModule,
    TranslateModule,
    NgIf,
    MatMenuModule
  ]
})
export class NavComponent implements OnInit {
  currentTextLink: string = '';
  canChangeLanguage: boolean = false;
  currentLanguage: string = '';

  constructor(
    private translate: TranslateService,
    private storage: StorageService,
    private _http: HttpClient
  ) {
  }

  ngOnInit() {
    this.currentLanguage = this.storage.getData('language')!;
  }

  changeLanguage(event: any) {
    this.storage.saveData('language', event.value);
    this.currentLanguage = this.storage.getData('language')!;
    this.translate.use(this.currentLanguage);
    this.canChangeLanguage = false;
  }
}

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

