import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../services/storage/storage.service';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  currentLanguage: string = '';


  constructor(
    private translate: TranslateService,
    private storage: StorageService,
    private _http: HttpClient
  ) {
  }

  ngOnInit() {
    this.currentLanguage = this.storage.getData('language')!;
  }
}

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

