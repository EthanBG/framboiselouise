import { MatButtonModule } from '@angular/material/button';
import { HomeRoutingModule } from './home-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { NavComponent } from "../nav/nav.component";
import {AboutmeComponent} from "../aboutme/aboutme.component";
import {FloristComponent} from "../florist/florist.component";

@NgModule({
  declarations: [HomeComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatButtonModule,
        TranslateModule,
        HttpClientModule,
        HomeRoutingModule,
        MatButtonToggleModule,
        MatIconModule,
        NavComponent,
        AboutmeComponent,
        FloristComponent,
    ]
})
export class HomeModule {}
