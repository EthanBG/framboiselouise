import {Component, HostListener, OnInit} from '@angular/core';
import {TranslateModule} from "@ngx-translate/core";

@Component({
  selector: 'app-aboutme',
  standalone: true,
  imports: [
    TranslateModule
  ],
  templateUrl: './aboutme.component.html',
  styleUrl: './aboutme.component.scss'
})
export class AboutmeComponent {

  ngAfterViewInit() {
    if (window.innerWidth > 768) {
      var creationTitleW = document.getElementById("creationTitle")!.offsetWidth
      document.getElementById("creationImg")!.style.width = (window.innerWidth - (200 + creationTitleW)).toString() + "px"
    }
    else {
      document.getElementById("creationImg")!.style.width = (window.innerWidth * 0.95).toString() + "px"
     }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    if (event.target.innerWidth > 768) {
      var creationTitleW = document.getElementById("creationTitle")!.offsetWidth
      document.getElementById("creationImg")!.style.width = (event.target.innerWidth - (100 + creationTitleW)).toString() + "px"
    } else  {
      document.getElementById("creationImg")!.style.width = (event.target.innerWidth * 0.95).toString() + "px"
    }
    }
}
