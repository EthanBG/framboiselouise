import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { StorageService } from './services/storage/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'FramboiseLouise';

  constructor(
    private translate: TranslateService,
    private router: Router,
    private storage: StorageService
  ) {}

  ngOnInit() {
    if (!this.storage.getData('language')) {
      this.storage.saveData('language', this.translate.getBrowserLang()!);
    }
    this.translate.setDefaultLang(this.storage.getData('language')!);

    if (!this.storage.getData('theme')) {
      if (window.matchMedia('(prefers-color-scheme: light)').matches) {
        this.storage.saveData('theme', 'light');
      } else {
        this.storage.saveData('theme', 'dark');
      }
    }
  }
}
